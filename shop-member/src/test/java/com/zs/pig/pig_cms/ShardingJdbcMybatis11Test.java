package com.zs.pig.pig_cms;

import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.zs.pig.common.utils.IpGetter;
import com.zs.pig.common.utils.PasswordEncoder;
import com.zs.pig.user.api.model.Member;
import com.zs.pig.user.api.service.MemberService;
import com.zs.pig.user.mapper.MemberMapper;

//http://blog.csdn.net/linuu/article/details/50929635
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:config/spring/spring-database.xml",
        "classpath*:config/spring/spring-sharding.xml" })
public class ShardingJdbcMybatis11Test {

    @Resource
    public MemberService MemberService;
    @Resource
    public MemberMapper MemberMapper;
    
  
    @Test
    public void testUserInsert() {
    	Member m=new Member();
		m.setEmail("email11");
		m.setUsername("username11");
		String secPwd = PasswordEncoder.encrypt("passs", "email");
	    m.setPhone("phone");
	    m.setStatus(1);
	    m.setAddress(IpGetter.getLocalIP());
	    m.setPassword(secPwd);
	    m.setAddtime(new Date());
	   m.setTruename(m.getUsername());
	   Random r = new Random();
	   m.setGold(r.nextInt(10));
       // u.setCreateBy("cc");
	   MemberService.insertSelective(m);
	   System.out.println("id:=============="+m.getId());
	   System.out.println("end.........");
    }
    
 
    @Test
    public void testFindAll(){
        List<Member> users = MemberMapper.select(new Member());
        if(null != users && !users.isEmpty()){
            for(Member u :users){
                System.out.println(u);
            }
        }
    }
    
    @Test
    public void testSQLIN(){
       
    }
    
    
    
    
}
